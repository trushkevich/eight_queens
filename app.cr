class App
  DEFAULT_BOARD_SIZE = 1

  def initialize(board_size)
    @board_size = board_size.to_i == 0 ? DEFAULT_BOARD_SIZE : board_size.to_i
    @queens_number = @board_size
    #   [1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1], [8, 1],
    #   [1, 2], [2, 2], [3, 2], [4, 2], [5, 2], [6, 2], [7, 2], [8, 2],
    #   [1, 3], [2, 3], [3, 3], [4, 3], [5, 3], [6, 3], [7, 3], [8, 3],
    #   [1, 4], [2, 4], [3, 4], [4, 4], [5, 4], [6, 4], [7, 4], [8, 4],
    #   [1, 5], [2, 5], [3, 5], [4, 5], [5, 5], [6, 5], [7, 5], [8, 5],
    #   [1, 6], [2, 6], [3, 6], [4, 6], [5, 6], [6, 6], [7, 6], [8, 6],
    #   [1, 7], [2, 7], [3, 7], [4, 7], [5, 7], [6, 7], [7, 7], [8, 7],
    #   [1, 8], [2, 8], [3, 8], [4, 8], [5, 8], [6, 8], [7, 8], [8, 8],
    # or
    #   a1 b1 c1 d1 e1 f1 g1 h1
    #   a2 b2 c2 d2 e2 f2 g2 h2
    #   a3 b3 c3 d3 e3 f3 g3 h3
    #   a4 b4 c4 d4 e4 f4 g4 h4
    #   a5 b5 c5 d5 e5 f5 g5 h5
    #   a6 b6 c6 d6 e6 f6 g6 h6
    #   a7 b7 c7 d7 e7 f7 g7 h7
    #   a8 b8 c8 d8 e8 f8 g8 h8
    @board = [] of Array(Int32)
    (1..@board_size).to_a.each do |row_num|
      (1..@board_size).to_a.each do |col_num|
        @board << [col_num, row_num]
      end
    end
  end

  def run
    t1 = Time.now
    puts "Total combinations: #{total_combinations}"
    combinations_number = 0
    @board.each_combination(@queens_number) do |combination|
      unless invalid_combination?(combination)
        combinations_number += 1
        print "\nCombination #{combinations_number} found:\n"
        print_board(combination)
      end
    end
    print "\n"
    puts "Done. Combinations found: #{combinations_number}"
    puts "Time: #{Time.now - t1}"
  end

  private def invalid_combination?(combination)
    horizontal_collision?(combination) ||
      vertical_collision?(combination) ||
      diagonal_collision?(combination)
  end

  private def horizontal_collision?(combination)
    # some queens stay on the same letter (column), hence number of uniq letters < than total letters
    combination.map{|c| c[0]}.uniq.size < @board_size
  end

  private def vertical_collision?(combination)
    # some queens stay on the same number (row), hence number of uniq letters < than total letters
    combination.map{|c| c[1]}.uniq.size < @board_size
  end

  private def diagonal_collision?(combination)
    combination.each_combination(2) do |pair|
      if (pair[0][0] - pair[1][0]).abs == (pair[0][1] - pair[1][1]).abs
        return true
      end
    end
  end

  private def total_combinations
    @board.each_combination(@queens_number).size
  end

  private def print_board(combination)
    puts " " + " _" * @board_size
    (1..@board_size).to_a.reverse.each do |row_num|
      print "#{row_num}|"
      (1..@board_size).to_a.each do |col_num|
        print "#{cell_value(combination, [col_num, row_num])}|"
      end
      print "\n"
    end
    chars = ("a".."z").to_a.first(@board_size).map {|c| " #{c}"}.join
    print " #{chars}\n"
  end

  private def cell_value(combination, cell)
    combination.each do |c|
      if c[0] == cell[0] && c[1] == cell[1]
        return "\e[4mw\e[0m"
      end
    end
    "_"
  end
end

board_size = ARGV.size > 0 ? ARGV[0].to_i : 0
App.new(board_size).run

# board_size 8
# Combinations found: 92
# Time: 04:33:19.5409640

# board_size 7
# Combinations found: 40
# Time: 00:02:50.9557540

# board_size 6
# Combinations found: 4
# Time: 00:00:03.8409730

# board_size 5
# Combinations found: 10
# Time: 00:00:00.1105190

# board_size 4
# Combinations found: 2
# Time: 00:00:00.0026340

# board_size 3
# Combinations found: 0
# Time: 00:00:00.0001820

# board_size 2
# Combinations found: 0
# Time: 00:00:00.0000700

# board_size 1
# Combinations found: 1
# Time: 00:00:00.0001140
