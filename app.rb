class App
  attr_reader :board_size, :board

  DEFAULT_BOARD_SIZE = 1

  def initialize(board_size)
    board_size = DEFAULT_BOARD_SIZE if board_size.to_i == 0
    #   [1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1], [8, 1],
    #   [1, 2], [2, 2], [3, 2], [4, 2], [5, 2], [6, 2], [7, 2], [8, 2],
    #   [1, 3], [2, 3], [3, 3], [4, 3], [5, 3], [6, 3], [7, 3], [8, 3],
    #   [1, 4], [2, 4], [3, 4], [4, 4], [5, 4], [6, 4], [7, 4], [8, 4],
    #   [1, 5], [2, 5], [3, 5], [4, 5], [5, 5], [6, 5], [7, 5], [8, 5],
    #   [1, 6], [2, 6], [3, 6], [4, 6], [5, 6], [6, 6], [7, 6], [8, 6],
    #   [1, 7], [2, 7], [3, 7], [4, 7], [5, 7], [6, 7], [7, 7], [8, 7],
    #   [1, 8], [2, 8], [3, 8], [4, 8], [5, 8], [6, 8], [7, 8], [8, 8],
    # or
    #   [?a, 1], [?b, 1], [?c, 1], [?d, 1], [?e, 1], [?f, 1], [?g, 1], [?h, 1],
    #   [?a, 2], [?b, 2], [?c, 2], [?d, 2], [?e, 2], [?f, 2], [?g, 2], [?h, 2],
    #   [?a, 3], [?b, 3], [?c, 3], [?d, 3], [?e, 3], [?f, 3], [?g, 3], [?h, 3],
    #   [?a, 4], [?b, 4], [?c, 4], [?d, 4], [?e, 4], [?f, 4], [?g, 4], [?h, 4],
    #   [?a, 5], [?b, 5], [?c, 5], [?d, 5], [?e, 5], [?f, 5], [?g, 5], [?h, 5],
    #   [?a, 6], [?b, 6], [?c, 6], [?d, 6], [?e, 6], [?f, 6], [?g, 6], [?h, 6],
    #   [?a, 7], [?b, 7], [?c, 7], [?d, 7], [?e, 7], [?f, 7], [?g, 7], [?h, 7],
    #   [?a, 8], [?b, 8], [?c, 8], [?d, 8], [?e, 8], [?f, 8], [?g, 8], [?h, 8],
    @board = []
    (1..board_size).to_a.each do |row_num|
      (1..board_size).to_a.each do |col_num|
        @board << [col_num, row_num]
      end
    end
    @board_size = board_size
    @queens_number = board_size
  end

  def run
    t1 = Time.now
    puts "Total combinations: #{total_combinations}"
    suitable_combinations = []
    iteration = 0
    begin
      combinations_enum = combinations.to_enum
      while true
        iteration += 1
        combination = combinations_enum.next
        # puts "Iteration ##{iteration}, time: #{Time.now - t1} seconds\r"
        # puts combination.inspect
        unless invalid_combination?(combination)
          puts "Suitable combination #{suitable_combinations.size + 1} found:"
          print_board(combination)
          suitable_combinations << combination
        end
      end
    rescue StopIteration
      puts ''
      puts "Done. Suitable combinations found: #{suitable_combinations.size}"
      # puts suitable_combinations.inspect
    end
    puts "Time: #{Time.now - t1} seconds."
  end

  private

  def invalid_combination?(combination)
    horizontal_collision?(combination) ||
      vertical_collision?(combination) ||
      diagonal_collision?(combination)
  end

  def horizontal_collision?(combination)
    # some queens stay on the same letter (column), hence number of uniq letters < than total letters
    combination.map(&:first).uniq.size < @board_size
  end

  def vertical_collision?(combination)
    # some queens stay on the same number (row), hence number of uniq letters < than total letters
    combination.map(&:last).uniq.size < @board_size
  end

  def diagonal_collision?(combination)
    pairs = combination.combination(2)
    begin
      while true
        pair = pairs.next
        if (pair[0][0] - pair[1][0]).abs == (pair[0][1] - pair[1][1]).abs
          return true
        end
      end
    rescue StopIteration
    end
  end

  def combinations
    @board.combination(@queens_number)
  end

  def total_combinations
    combinations.size
  end

  def print_board(combination)
    puts ' ' + ' _' * @board_size
    (1..@board_size).to_a.reverse.each do |row_num|
      print "#{row_num}|"
      (1..@board_size).to_a.each do |col_num|
        print "#{cell_value(combination, [col_num, row_num])}|"
      end
      puts ''
    end
    print_letters
  end

  def print_letters
    print ' '
    letter = ' a'
    @board_size.times do
      print letter
      letter = letter.next
    end
    puts ''
  end

  def cell_value(combination, cell)
    combination.include?(cell) ? "\e[4mw\e[0m" : '_'
  end
end

App.new(ARGV[0].to_i).run

# board_size 7
# Suitable combinations found: 40
# Time: 306.758642644 seconds.

# board_size 6
# Suitable combinations found: 4
# Time: 3.839040772 seconds.

# board_size 5
# Suitable combinations found: 10
# Time: 0.110102859 seconds.

# board_size 4
# Suitable combinations found: 2
# Time: 0.004863366 seconds.

# board_size 3
# Suitable combinations found: 0
# Time: 0.00031499 seconds.

# board_size 2
# Suitable combinations found: 0
# Time: 0.000130289 seconds.

# board_size 1
# Suitable combinations found: 1
# Time: 0.000126217 seconds.
